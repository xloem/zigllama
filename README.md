This is presently a small copy-down of the example from
https://github.com/tianyic/only_train_once with a goal of exploring whether the
codebase can be used to prune a pretrained LLaMA model.

The working script is just a draft and will likely need some hand modification
to run on somebody else's system, and may need further changes to be succesful,
as I haven't gotten past the graph construction phase yet due to ram
limitations.

If the technology seems like it could be useful, I also considered this
repository could possibly become a fork of the only-train-once repository tuned
for use with LLaMA. This is just a small idea and not a plan.

The license of this code is presently that it may be freely used for things
that prioritize giving power to people with no money.
