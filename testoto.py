print('importing modules ...')
import torch, transformers, accelerate, sentencepiece, only_train_once, datasets, tqdm
import sys
sys.setrecursionlimit(16384)

print('loading model ...')
MODEL = 'decapoda-research/llama-7b-hf'
tokenizer = transformers.LlamaTokenizer.from_pretrained(MODEL)
pipe = transformers.pipeline('text-generation', MODEL, tokenizer=tokenizer, device_map='auto', torch_dtype=torch.float16)
model = pipe.model
del pipe

print('creating OTO ...')
dummy_input = tuple(dict(
        #input_ids = None,
        input_ids = torch.zeros(1,2048, dtype=int),
        attention_mask = torch.ones(1,2048, dtype=int),
        position_ids = None,
        past_key_values = None,
        #inputs_embeds = torch.zeros(1,2048,4096),
        inputs_embeds = None,
        labels = None,
        use_cache = None,
        output_attentions = None,
        output_hidden_states = None,
        return_dict = None,
    ).values())
oto = only_train_once.OTO(model=model, dummy_input=dummy_input)

print('writing zig pdf ...')
try:
    oto.visualize_zigs()
except:
    print('probably missing graphviz')

print('creating dhspg ...')
optimizer = oto.dhspg(
    variant='sgd', # optimizer used for training baseline full model
    lr=0.1, # initial learning rate
    weight_decay=0.0, #1e-4
    start_pruning_steps=0, #50 * len(trainloader) to prune after 50 epochs
    epsilon=0.0, #0.95 how aggressively to explore group sparsity
    target_group_sparsity=0.7,
    # there are more hyperparameters
)

print('loading dataset ...')
dataset = datasets.load_dataset('c4', 'en', streaming=True)
traindata = dataset['train'].shuffle(seed=0, buffer_size=10240).take(128)
trainloader = torch.utils.data.DataLoader(traindata, batch_size=1)#4)

print('training ...')
model.train()
#def criterion(y_pred, y):
#    return torch.nn.CrossEntropyLoss(y_pred[...,:-1].flatten(), y[...,1:].flatten())
for epoch in range(1):
    with tqdm.tqdm(trainloader) as pbar:
        for data in pbar:
            inputs = data['text']
            encoded_inputs = tokenizer(inputs, return_tensors='pt')
            loss = model.forward(**encoded_inputs, labels=encoded_inputs['input_ids'])[0]
            pbar.set_description(str(loss.item()))
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

print('compressing ...')
print(oto.compress())
